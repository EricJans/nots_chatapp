>>>>>
Studentnaam:Eric Jans

Studentnummer: 509967
---
#Algemene beschrijving applicatie
Multi-client chat applicatie gebaseerd op het TCP protocol
Geschreven in C#

##Generics
###Beschrijving van concept in eigen woorden
Generics zijn klassen waarbij datatypes als parameter mee kunnen worden gegeven. 
Een voorbeeld van een generic is een List<T> in C#.
Bij het definieren van een List word een datatype meegegeven. hierdoor wordt als het ware een restrictie gelegd, 
enkel het gegeven datatype kan toegevoegd worden aan de lijst

###Code voorbeeld van je eigen code
Ik heb op meerdere plaatsen List gebruikt, een van deze voorbeelden is op de server. 
Deze houd een lijst van Clients bij:

private List<Client> ConnectedClients;
ConnectedClients = new List<Client>();


###Alternatieven & adviezen
Boxing en unboxing, al kan ik deze niet aanraden. Boxing en unboxing werkt in het algemeen trager dan Generics.


###Authentieke en gezaghebbende bronnen
https://msdn.microsoft.com/en-us/library/ms379564(v=vs.80).aspx

##Boxing & Unboxing
###Beschrijving van concept in eigen woorden
bij boxing word een prime variable, ofwel een int, string, bool
omgezet naar een Object die op de heap wordt opgeslagen

###Code voorbeeld van je eigen code
Ik heb in mijn code geen boxing/unboxing gebruikt, dus kan hier geen voorbeeld van mijn eigen code geven

###Alternatieven & adviezen
Een alternatief voor boxing en unboxing zijn generics. Mijn advies is ook om generics te gebruiken.
Generics vermijd het boxen en unboxen van data, wat een stuk trager is.

###Authentieke en gezaghebbende bronnen
https://msdn.microsoft.com/en-us/library/ms379564(v=vs.80).aspx
https://msdn.microsoft.com/en-us/library/yz2be5wk.aspx

##Delegates & Invoke
###Beschrijving van concept in eigen woorden
een delegate is een type dat word gebruikt om naar methodes te refereren. 
Door een return waarde en parameters aan te geven kan elke methode aan de delegate worden toegevoegd, 
zolang de parameters en het return type van de methode gelijk is aan die van de delegate. 
Delegates worden gebruikt om methodes door te geven als parameters aan methodes.

Delegates worden vaak gebruikt in combinatie met de Invoke functie. 
Door een control te invoken wordt de delegate die meegegeven is aan de Invoke methode uitgevoerd op de thread waar de control zich op bevind.

###Code voorbeeld van je eigen code
Ik heb delegates en invokes gebruikt voor het plaatsen van een bericht op de chatapp:


public void WriteInput(string input)
        {
            if (lstBox.InvokeRequired)
            {
                Invoke(new MessageDelegate(WriteInput), new Object[] { input });
            }
            else
            {
                lstBox.Items.Add(input);
            }
        }

public delegate void MessageDelegate(string input);


De delegate wordt op verschillende plaatsen aangeroepen in de invoke methode, maar op dezelfde manier zoals hierboven vermeld.

###Alternatieven & adviezen
Een alternatief voor invoke is BeginInvoke, dit doet hetzelfde als de normale invoke alleen dan asynchroon.
In plaats van delegates kan ook een interface gebruikt worden. 

Er zijn momenten wanneer je beter een interface kan gebruiken dan een delegate.
een voorbeeld is dat een class maar één implementatie nodig heeft van een methode, of als er een groep gerelateerde methoden zijn die gebruikt kunnen worden

###Authentieke en gezaghebbende bronnen
https://msdn.microsoft.com/en-us/library/zyzhdc6b(v=vs.110).aspx
https://msdn.microsoft.com/en-us/library/ms173173.aspx
https://msdn.microsoft.com/en-us/library/a06c0dc2(v=vs.110).aspx


##Threading & Async
###Beschrijving van concept in eigen woorden

een thread kan worden gezien als een proces. door meerdere threads aan te maken in een programma kunnen meer taken naast elkaar uitgevoerd worden.
Async is in het opzicht van meerdere taken tegelijk uitvoeren hetzelfde als threads, maar terwijl threads ze op het zelfde moment naast elkaar uit kan voeren,
doet async dit niet. Async werkt in een methode tot een bepaald punt, maar kan "wachten" met deze methode wanneer het nodig is en met wat anders verder gaan.
Een voorbeeld hiervan is de UI in de chat applicatie. Je wilt niet dat deze vastloopt, dus gebruik je Async.

###Code voorbeeld van je eigen code
Async heb ik gebruikt voor het toevoegen van clients, zodat de server GUI niet vastloopt


private void HandleAsyncConnection(IAsyncResult res)
        {
            StartAccept();
            TcpClient TcpClient = _TcpListener.EndAcceptTcpClient(res);
            Client client = new Client(_form, lstBox);
            client._NetworkStream = TcpClient.GetStream();
            client._TcpClient = TcpClient;

            _form._TcpClient = TcpClient;

            ConnectedClients.Add(client);
            Thread _Thread = new Thread(new ParameterizedThreadStart(_form.ReceiveData));
            _Thread.Start(client);
        }


Tevens heb ik in het bovenstaande stukje code ook Threads gebruikt. De thread wordt in dit stukje code gebruikt bij het maken van een nieuwe client verbinding.
Dit wordt gedaan zodat de server naar meerdere clients tegelijk kan luisteren en zodat er meerdere clients tegelijk met een server verbonden kunnen zijn.

###Alternatieven & adviezen
Een alternatief voor threads is Task. De Task klasse is een onderdeel van de Threading library. 
Hieronder is een voorbeeld te zien van een Task, door middel van lambda word een stuk code meegegeven aan het instantieren van de Task.

Task t = Task.Run( () => {
                                  // Just loop.
                                  int ctr = 0;
                                  for (ctr = 0; ctr <= 1000000; ctr++)
                                  {}
                                  Console.WriteLine("Finished {0} loop iterations",
                                                    ctr);
                               } );

Code is van https://msdn.microsoft.com/en-us/library/system.threading.tasks.task(v=vs.110).aspx

###Authentieke en gezaghebbende bronnen
https://msdn.microsoft.com/en-us/library/system.threading.tasks.task(v=vs.110).aspx
https://msdn.microsoft.com/en-us/library/mt674882.aspx
https://msdn.microsoft.com/en-us/library/system.threading.thread(v=vs.110).aspx

<<<<<