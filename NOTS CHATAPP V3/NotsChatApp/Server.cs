﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace NotsChatApp
{
    class Server
    {
        private TcpListener _TcpListener;
        private Form1 _form;
        private ListBox lstBox;
        private NetworkStream _Stream;
        private List<Client> ConnectedClients;

        public Server(Form1 form, ListBox lstbox)
        {
            ConnectedClients = new List<Client>();
            this._form = form;
            this.lstBox = lstbox;
        }

        /// <summary>
        /// Methode die de server laat luisteren naar inkomende clients
        /// </summary>
        public void Listener()
        {
            //Checken of de server opgestart kan worden en clients geaccepteerd kunnen worden
            try
            {
                _TcpListener = new TcpListener(IPAddress.Any, 9000);
                _TcpListener.Start();
                lstBox.Items.Add("Listening for clients...");
                StartAccept();
            }
            //IP of poort al in gebruik, server kan niet opstarten
            catch(SocketException e)
            {
                _form.setMessage("This port/IP is already in use. Server could not start");
            }
        }

        /// <summary>
        /// Methode die een client verwijderd die uit de chat is gegaan
        /// </summary>
        /// <param name="c">Client die uit de chat gegaan is</param>
        public void removeClient(Client c)
        {
            ConnectedClients.Remove(c);
        }

        /// <summary>
        /// Methode waarin clients geaccepteerd worden
        /// </summary>
        private void StartAccept()
        {
            _TcpListener.BeginAcceptTcpClient(HandleAsyncConnection, _TcpListener);
        }

        /// <summary>
        /// Methode om Clients Async toe te voegen zodat de GUI niet vastloopt.
        /// </summary>
        /// <param name="res">resultaat van de methode, de client</param>
        private void HandleAsyncConnection(IAsyncResult res)
        {
            StartAccept();
            TcpClient TcpClient = _TcpListener.EndAcceptTcpClient(res);
            Client client = new Client(_form, lstBox);
            client._NetworkStream = TcpClient.GetStream();
            client._TcpClient = TcpClient;

            _form._TcpClient = TcpClient;

            ConnectedClients.Add(client);
            Thread _Thread = new Thread(new ParameterizedThreadStart(_form.ReceiveData));
            _Thread.Start(client);
        }

        /// <summary>
        /// Versturen van een bericht naar alle users die verbonden zijn
        /// </summary>
        /// <param name="message">het bericht dat verzonden moet worden</param>
        public void BroadCastMessage(string message)
        {
            //Lopen door alle verbonden clients
            for (int Connections = 0; Connections < ConnectedClients.Count; Connections++)
            {
                _form._TcpClient = ConnectedClients[Connections]._TcpClient;

                //Proberen het bericht naar iedereen te versturen
                try
                {
                    _Stream = ConnectedClients[Connections]._TcpClient.GetStream();
                    byte[] ByteMessage = new byte[1024];
                    ByteMessage = Encoding.ASCII.GetBytes(message);
                    _Stream.Write(ByteMessage, 0, message.Length);
                }
                //Er is een gebruiker niet helemaal verwijderd, deze wordt hierbij nog weggehaald
                catch(Exception e)
                {
                    ConnectedClients.RemoveAt(Connections);
                }
            }
        }
    }
}
