﻿using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace NotsChatApp
{
    class Client
    {
        public TcpClient _TcpClient;
        private Thread _Thread;
        public NetworkStream _NetworkStream;
        private ListBox lstBox;
        private Form1 _form;

        public Client(Form1 form, ListBox lstBox)
        {
            this.lstBox = lstBox;
            this._form = form;
        }
        /// <summary>
        /// Methode waarmee gezocht wordt naar een server en hier mee verbonden wordt.
        /// </summary>
        /// <returns>of de client verbonden is of niet</returns>
        public bool Connect()
        {
            lstBox.Items.Add("Trying to connect...");
            //Proberen te verbinden met de server
            try
            {
                _TcpClient = new TcpClient(_form.getIP(), 9000);
                _form._TcpClient = _TcpClient;
                _Thread = new Thread(new ParameterizedThreadStart(_form.ReceiveData));
                _Thread.Start(this);
                return true;
            }
            //Er was geen server gevonden
            catch (SocketException e)
            {
                _form.setMessage("There was no server available at the given IP");
                return false;
            }
        }
    }
}
