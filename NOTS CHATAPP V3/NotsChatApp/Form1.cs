﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace NotsChatApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public TcpClient _TcpClient;
        public NetworkStream _Stream;
        private Client _Client;
        private Server _Server;

        public delegate void MessageDelegate(string input);

        /// <summary>
        /// Methode die gebruikt wordt om berichten op het scherm te schrijven
        /// </summary>
        /// <param name="input">Het bericht dat afgedrukt moet worden</param>
        public void WriteInput(string input)
        {
            if (lstBox.InvokeRequired)
            {
                Invoke(new MessageDelegate(WriteInput), new Object[] { input });
            }
            else
            {
                lstBox.Items.Add(input);
            }
        }

        private void ListenButton_Click(object sender, EventArgs e)
        {
            disableClientButtons();
            _Server = new Server(this, lstBox);
            _Server.Listener();
        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            //Afvangen van het versturen van een bericht terwijl je niet verbonden bent
            try
            {
                SendMessage();
                MessageBox.Text = "";
                MessageBox.Focus();
            }
            catch(Exception notConnected)
            {
                setMessage("You are not connected to a server yet");
            }       
        }

        /// <summary>
        /// Wordt gebruikt om een bericht af te drukken
        /// </summary>
        /// <param name="message">bericht</param>
        public void setMessage(string message)
        {
            Invoke(new MessageDelegate(WriteInput), new Object[] { message });
        }

        /// <summary>
        /// Methode die controleert of er een nieuw bericht binnen komt, en als dit bericht binnenkomt wordt het uitgelezen
        /// en vervolgens op het scherm afgedrukt
        /// </summary>
        /// <param name="_chatClient">Instantie van een client</param>
        public void ReceiveData(object _chatClient)
        {
            string message;
            byte[] buffer;
            Invoke(new MessageDelegate(WriteInput), new Object[] { "Connected" });
            //Zolang de client/server verbonden is
            while (true)
            {
                string msg = "";
                List<string> messageToPrint = new List<string>();
                Client client = (Client)_chatClient;
                byte [] tempBuffer = new byte[1024];
                //opvangen van de stream voor het geval deze niet meer bestaat
                try
                {
                    _Stream = client._TcpClient.GetStream();
                }
                catch(Exception e)
                {
                    break;
                }
                //Client-only gedeelte van de methode
                if(_Server == null)
                {
                    _Stream.Read(tempBuffer, 0, tempBuffer.Length);
                    buffer = replaceNull(tempBuffer);
                    message = Encoding.ASCII.GetString(buffer);
                    Invoke(new MessageDelegate(WriteInput), new Object[] { message });
                }
                //Server only gedeelte van de methode, uitlezen van de data
                //en het bericht in elkaar plakken. dit bericht wordt vervolgens doorgestuurd
                if (_Server != null)
                {
                    while (_Stream.DataAvailable)
                    {
                        _Stream.Read(tempBuffer, 0, tempBuffer.Length);
                        buffer = replaceNull(tempBuffer);
                        message = Encoding.ASCII.GetString(buffer);
                        messageToPrint.Add(message);

                        for (int msgLength = 0; msgLength < messageToPrint.Count; msgLength++)
                        {
                            msg += messageToPrint[msgLength];
                        }
                    }
                    _Server.BroadCastMessage(msg);
                }
                
            }
        }

        /// <summary>
        /// Methode die door de meegegeven array heen loopt, van eind tot begin, tot er een andere waarde dan null wordt gevonden
        /// wanneer dit gebeurt wordt het bericht van begin tot deze waarde gekopieerd en teruggegeven
        /// </summary>
        /// <param name="b">Arraylist van bytes</param>
        /// <returns>de byte array zonder null waarden</returns>
        private byte[] replaceNull(byte[] b)
        {
            byte[] tempList = b;
            int i = tempList.Length - 1;
            while (tempList[i] == (byte)0)
            {
                --i;
                if(i == -1)
                {
                    break;
                }
            }
            byte[] newList = new byte[i + 1];
            Array.Copy(tempList, newList, i + 1);
            return newList;
        }

        /// <summary>
        /// Methode voor het versturen van Berichten
        /// </summary>
        private void SendMessage()
        {
            int bufferSize;
            int.TryParse(bufferBox.Text, out bufferSize);
            byte[] message;
            List<string> MessageList = new List<string>();

            //Check of het bericht in de messagebox groter is dan de buffer size
            if (MessageBox.Text.Length > bufferSize)
            {
                splitMessage(MessageList, bufferSize);
                //Client-only deel
                if (_Server == null)
                {
                    //Het versturen van het gesplitste bericht
                    for(int msgLength = 0; msgLength < MessageList.Count; msgLength++)
                    {
                        message = new byte[bufferSize];
                        message = Encoding.ASCII.GetBytes(MessageList[msgLength]);
                        _Stream.Write(message, 0, message.Length);
                    }
                    //Disconnecten en close van de client wanneer "bye" wordt ontvangen
                    if (MessageBox.Text.Equals("bye"))
                    {
                        setMessage("Disconnected");
                        _Stream.Close();
                        _TcpClient.Close();
                    }
                }
                //Server gedeelte, broadcasten van het bericht
                else
                {
                    _Server.BroadCastMessage(MessageBox.Text);
                    Invoke(new MessageDelegate(WriteInput), new Object[] { MessageBox.Text });
                }
            }
            //Bericht is niet langer dan de buffer size
            else
            {
                //Client-only deel. het versturen van een bericht naar de server
                if (_Server == null)
                {
                    message = Encoding.ASCII.GetBytes(MessageBox.Text);
                    _Stream.Write(message, 0, message.Length);
                    if (MessageBox.Text.Equals("bye"))
                    {
                        setMessage("Disconnected");
                        _Stream.Close();
                        _TcpClient.Close();
                    }
                }
                //Server-only deel. Bericht van de server naar alle clients sturen
                else
                {
                    _Server.BroadCastMessage(MessageBox.Text);
                    Invoke(new MessageDelegate(WriteInput), new Object[] { MessageBox.Text });
                }
            }
        }

        /// <summary>
        /// Methode die het te versturen bericht opsplitst gebaseerd op de buffersize
        /// </summary>
        /// <param name="messageList">Lijst van (deel)berichten</param>
        /// <param name="split">waarde waar het bericht op gesplitst moet worden(buffer size)</param>
        private void splitMessage(List<string> messageList, int split)
        {
            string msg = MessageBox.Text;
            int splitAt = split;
            for(int i = 0; i < msg.Length; i = i + splitAt)
            {
                //Bericht opsplitsen zolang het bericht langer is dan de buffersize
                if(msg.Length - i >= splitAt)
                {
                    messageList.Add(msg.Substring(i, splitAt));
                }
                //Bericht opsplitsen als het bericht minder lang is dan de buffersize(restant)
                else
                {
                    messageList.Add(msg.Substring(i));
                }
            }
        }


        private void ConnectButton_Click(object sender, EventArgs e)
        {
            //Kijken of de buffer een getal is
            if (validateBuffer())
            {
                _Client = new Client(this, lstBox);
                if (_Client.Connect())
                {
                    disableClientButtons();
                }       
            }
            else
            {
                bufferBox.Text = "1024";
                System.Windows.Forms.MessageBox.Show("The default buffer of 1024 has been used since no valid buffer size has been given");
                _Client = new Client(this, lstBox);
                if (_Client.Connect())
                {
                    disableClientButtons();
                };
            }
        }

        private void MessageBox_KeyUp(object sender, KeyEventArgs e)
        {
            //als de ingedrukte key enter is, wordt het bericht verstuurd
            if(e.KeyCode == Keys.Enter)
            {
                try
                {
                    SendMessage();
                    MessageBox.Text = "";
                    MessageBox.Focus();
                    autoScrollDown();
                }
                //Opvangen voor het verzenden van een bericht met Enter terwijl je niet verbonden bent
                catch(Exception NotConnected)
                {
                    setMessage("You are not connected to a server yet");
                }
            }
        }

        /// <summary>
        /// Methode die er voor zorgt dat je automatisch naar beneden scrollt
        /// </summary>
        private void autoScrollDown()
        {
            lstBox.SelectedIndex = lstBox.Items.Count - 1;
            lstBox.SelectedIndex = -1;
        }

        /// <summary>
        /// Uitschakelen van de GUI buttons
        /// </summary>
        private void disableClientButtons()
        {
            ConnectButton.Enabled = false;
            ListenButton.Enabled = false;
            bufferBox.Enabled = false;
        }

        /// <summary>
        /// Inschakelen van de GUI buttons
        /// </summary>
        public void enableClientButtons()
        {
            ConnectButton.Enabled = true;
            ListenButton.Enabled = true;
            bufferBox.Enabled = true;
        }

        /// <summary>
        /// Methode die kijkt of er alleen nummers in de buffer zitten. 
        /// </summary>
        /// <returns>of buffer alleen nummers of ook letters is</returns>
        private Boolean validateBuffer()
        {
            int parsedValue;
            //Als bufferbox leeg is:
            if (bufferBox.Text.Equals(null))
            {
                return false;
            }
            //Als de buffer size groter dan 0 is en alleen getallen is
            else if (int.TryParse(bufferBox.Text, out parsedValue) && parsedValue > 0)
            {
                return true;
            }
            else
            {
                return false;
            }  
        }

        /// <summary>
        /// Methode waarin het IP address op wordt gehaald voor de client
        /// </summary>
        /// <returns>Opgegeven IP address</returns>
        public string getIP()
        {
            return IPAddress.Text;
        }
    }
}
